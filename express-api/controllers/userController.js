const userService = require('../services/userService');
const bcrypt = require("bcrypt");

const createUser =  async (req, res) => {
    const body = req.body;
    const salt = bcrypt.genSaltSync(10);

    try {
        body.password = bcrypt.hashSync(body.user_password, salt);
        
        // handle dup entry
        const newUser = await userService.createUser(body);
        return res.status(200).json({
            success: 1,
            userId: newUser
        });
    } catch(err) {
        console.log('Error creating user. ', err);
        return res.status(500).json({
            success: 0,
            message: 'Error creating user'
        });
    }
}

module.exports = {
    createUser
};