require('dotenv').config({ path: require('find-config')('.env')});
const mysql = require('mysql2/promise');

let pool;
const createPool = async () => {
  pool = await mysql.createPool({
    connectionLimit: 10,
    user: process.env.user,
    host: process.env.host,
    password: process.env.password,
    port: process.env.port, 
    multipleStatements: true
  });
};


module.exports = async () => {
    try {
        if (!pool) await createPool();
    } catch(err) {
        throw new Error('Database could not be initialized.')
    }   
    
    return pool;
};
