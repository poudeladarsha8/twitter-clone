var express = require("express");
const getPool = require('./config/db');
const app = express();
const userRouter = require('./routes/userRoutes');

// connect routes
app.use(express.json());
app.use("/users",  userRouter);

// startup routines
const fs = require('fs');
const path = require('path');

const sqlFilePath = path.join(__dirname, '/config/startUpScript.sql');
const startupScript = fs.readFileSync(sqlFilePath, 'utf8');

const start = async () => {
  console.log('Loading startup routines.');
  try {
    const pool = await getPool();
    await pool.query(startupScript);
    app.listen(8080, () => {
      console.log('Server is up and running.')
    }); 
  } catch(err) {
    console.log('Failed to execute startup routines.');
    console.log(err);
  }
};

start();