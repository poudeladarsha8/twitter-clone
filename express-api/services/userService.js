const getPool = require('../config/db');
const fs = require("fs");

const createUser = async (userData) => {
  const { user_name, user_password, firstname, lastname, email, phone } =
    userData;

  const pool = await getPool();
  let connection;

  try {
    connection = await pool.getConnection();
    await connection.beginTransaction();
    
    // user table
    const [result] = await connection.execute(
      `INSERT INTO USER(user_name, user_password)
                values(?,?)`,
      [user_name, user_password]
    );

    // user info table
    await connection.execute(
      `INSERT INTO USER_INFO(user_name, firstname, lastname, email, phone)
                values(?,?,?,?,?)`,
      [user_name, firstname, lastname, email, phone]
    );

    await connection.commit();

    return result.insertId;
  } catch (err) {
    if (connection) {
      connection.rollback();
    }
    throw err;
  } finally {
    if(connection) {
      connection.release();
    }
  }
};

module.exports = {
  createUser,
};


