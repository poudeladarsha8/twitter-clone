import React from "react";
import SignInConnector from "./sign-in/components/SignInConnector";
import SignUp from "./sign-up/components/SignUp";
import "./App.css";
import { Route, Routes } from "react-router-dom";

const App = () => (
  <main>
    <Routes>
      <Route path="/" element={<SignInConnector />} />
      <Route path="/signup" element={<SignUp />} />
    </Routes>
  </main>
);

export default App;