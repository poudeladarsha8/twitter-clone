import * as signInManagement from '../duck/signInDuck';
import * as signInService from '../services/signInService';

export const login = () => 
    async (dispatch, getState) => {
        console.log("here");
        const username = signInManagement.selectors.getUsername(getState());
        const password = signInManagement.selectors.getPassword(getState());

        try {
            const jwt = await signInService.login(username, password);
            dispatch(signInManagement.actions.reset);
        } catch(err) {
            console.log('Error in thunk while logging in, ', err);
        }
    }

