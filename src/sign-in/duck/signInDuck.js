import { ResetTv } from "@mui/icons-material";

const initialState = {
  username: "",
  password: "",
  usernameError: "",
  passwordError: ""
};

export const selectors = {
  getUsername: state => state.signIn.username,
  getPassword: state => state.signIn.password,
  getUsernameError: state => state.signIn.usernameError,
  getPasswordError: state => state.signIn.passwordError
};

const SET_USERNAME = "sign-in/username";
const SET_PASSWORD = "sign-in/password";
const SET_USERNAME_ERROR = "sign-in/username_error";
const SET_PASSWORD_ERROR = "sign-in/password_error";
const RESET = "sign-in/reset";

export const actions = {
  setUsername: (username) => ({
    type: SET_USERNAME,
    username
  }),
  setPassword: (password) => ({
    type: SET_PASSWORD,
    password,
  }),
  setUsernameError: (usernameError) => ({
    type: SET_USERNAME_ERROR,
    usernameError,
  }),
  setPasswordError: (passwordError) => ({
    type: SET_PASSWORD_ERROR,
    passwordError,
  }),
  reset: () => ({
    type: RESET 
  })
};

const signInReducer = (state = initialState, action) => {
  if(action.type == RESET) {
    return initialState;
  }

  if (action.type === SET_USERNAME) {
    return {
      ...state,
      username: action.username,
    };
  }

  if (action.type === SET_PASSWORD) {
    return {
      ...state,
      password: action.password
    };
  }

  if (action.type === SET_USERNAME_ERROR) {
    return {
      ...state,
      usernameError: action.usernameError,
    };
  }

  if (action.type === SET_PASSWORD_ERROR) {
    return {
      ...state,
      passwordError: action.passwordError
    };
  }

  return state;
};

export default signInReducer;
