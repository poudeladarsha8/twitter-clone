import { connect } from "react-redux";
import SignIn from "./SignIn";
import * as signInDuck from "../duck/signInDuck";
import {login} from '../thunk/login';

const mapStateToProps = (state) => ({
  username: signInDuck.selectors.getUsername(state),
  password: signInDuck.selectors.getPassword(state),
  usernameError: signInDuck.selectors.getUsernameError(state),
  passwordError: signInDuck.selectors.getPasswordError(state)
});

const dispatchToProps = {
  setUsername: signInDuck.actions.setUsername,
  setPassword: signInDuck.actions.setPassword,
  setUsernameError: signInDuck.actions.setUsernameError,
  setPasswordError: signInDuck.actions.setPasswordError,
  reset: signInDuck.actions.reset,
  login
};

const SignInConnector = connect(mapStateToProps, dispatchToProps)(SignIn);

export default SignInConnector;
