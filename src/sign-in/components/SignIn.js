import React from "react";
import FormInputField from "../../common/components/FormInputField";
import PropTypes from "prop-types";
import "../css/SignIn.css";
import ButtonWrapper from "../../common/components/ButtonWrapper";
import { Link } from "react-router-dom";


const SignIn = (props) => {
  return (
  <div className="sign-in-container">
    <FormInputField
      className="sign-in-username"
      label="Phone, email, or username"
      value={props.username}
      onChange={props.setUsername}
      errorMsg={props.usernameError}
      setError={props.setUsernameError}
    />

    <FormInputField
      className="sign-in-password"
      type="password"
      label="Password"
      value={props.password}
      onChange={props.setPassword}
      errorMsg={props.passwordError}
      setError={props.setUsernameError}
    />

    <ButtonWrapper name="LOG IN" onClick={props.login}/>

    <Link to="/signup" onClick={props.reset}>
      <span>Dont have an account? Sign up!</span>
    </Link>
  </div>
)};

SignIn.propTypes = {
  error: PropTypes.string,
  label: PropTypes.string,
};

export default SignIn;
