import React from "react";
import PropTypes from "prop-types";
import { Button } from "@mui/material";

const ButtonWrapper = (props) => (
  <div className={`button-wrapper ${props.className}`}>
    <Button
      variant="contained"
      disabled={props.disabled}
      onClick={props.onClick}
      fullWidth
    >
      {props.name}
    </Button>
  </div>
);

export default ButtonWrapper;
