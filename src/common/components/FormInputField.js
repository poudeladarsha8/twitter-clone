import React from "react";
import PropTypes from "prop-types";
import { TextField } from "@mui/material";


const FormInputField = (props) => {
  return(
  <div className={`form-input-field ${props.className}`}>
    <TextField
      value={props.value}
      onChange={e => props.onChange(e.target.value)}
      error={props.errorMsg ? true : false}
      helperText={props.errorMsg}
      label={props.label}
      type={props.type}
      fullWidth
    />
  </div>
)};

FormInputField.defaultProps = {
  disabled: false, 
  label: ""
}

FormInputField.propTypes = {
  label: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  errorMsg: PropTypes.string.isRequired,
  disabled: PropTypes.bool
};

export default FormInputField;
